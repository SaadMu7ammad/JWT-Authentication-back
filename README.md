# JWT-Authentication

## an application on jwt from scratch to apply what i've learned with mern stack
## [To The Front Repo] (https://gitlab.com/SaadMu7ammad/JWT-Authentication-front)
create a simple login/register website that used to store your tasks 

first you must register with email after that will recieve a hello email and emails are unique

then will redirect you to the login page 

also if you forgot your password you can reset it via email

there is 2 sections one for you tasks and another for all people registered on the website 

there is live updating means that if a user delete or edit his task will be deleted/edited also on All page tasks directly

for the another users
no one can edit or delete others tasks
only your tasks !

challenges been done through the project

- login page design from here (thanks to https://codepen.io/fastroware/pen/BaGRGyN) ~~done~~
- save users using mongoose ~~done~~
- password hashing ~~done~~
- express-validator with alert ~~done~~
- can reset the password via email ~~done~~
- weclome msg via email after registering ~~done~~
- register/login/reset with react ~~done~~
- jwt auth ~~done~~
- each user add and edit and delete his own products ~~done~~
- use redux/state to stop reloading after deleting or updating ~~done~~
- live updating ~~done~~

login page

![image](imgs/1.png)

register page

![image](imgs/2.png)

reset and add new password

![image](imgs/3.png)

![image](imgs/4.png)

home page

![image](imgs/5.png)

all page

![image](imgs/6.png)
